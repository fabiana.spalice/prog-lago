/* SCROLLNAVBAR */

window.onscroll = function(event) {
    let navbar = document.querySelector(".lago-navbar")
    //console.log(document.documentElement.scrollTop)
    if (document.documentElement.scrollTop > 50) {
        navbar.classList.add("padding-extension")
    } else {
        navbar.classList.remove("padding-extension")
    }
}

/* CAROUSEL */

document.querySelectorAll(".carousel").forEach(carousel => {
    const items = carousel.querySelectorAll(".carousel__item");
    const buttonsHTML = Array.from(items, () => {
        return `<span class="carousel__button"></span>`;
    })

    var htmlString = `<div class="carousel__nav">${ buttonsHTML.join("") }</div>`

    var htmlObject = document.createElement('div');

    htmlObject.innerHTML=htmlString;

    carousel.insertAdjacentElement("beforeend", htmlObject);

    const buttons = carousel.querySelectorAll(".carousel__button");
    buttons.forEach((button, i,) => {
        button.addEventListener("click", () => {
            // deseleziono gli item
            items.forEach(item => item.classList.remove("carousel__item--selected"));
            buttons.forEach(button => button.classList.remove("carousel__button--selected"));

            items[i].classList.add("carousel__item--selected");
            button.classList.add("carousel__button--selected");
        });
    });

    items[0].classList.add("carousel__item--selected");
    buttons[0].classList.add("carousel__button--selected");
});

/* TABS */

const tabs = document.querySelectorAll('[data-tab-target]')
const tabContents = document.querySelectorAll('[data-tab-content]')

tabs.forEach(tab => {
    tab.addEventListener('click', () => {
        const target = document.querySelector(tab.dataset.tabTarget)
        tabContents.forEach(tabContent => {
            tabContent.classList.remove('active')
        })
        tabs.forEach(tab => {
            tab.classList.remove('active')
        })
        tab.classList.add('active')
        target.classList.add('active')
    })
})